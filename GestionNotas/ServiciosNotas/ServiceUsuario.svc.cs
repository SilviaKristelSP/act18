﻿using ServiciosNotas.Modelo.Dao;
using ServiciosNotas.Modelo.Poco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiciosNotas
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ServiceUsuario" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ServiceUsuario.svc o ServiceUsuario.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ServiceUsuario : IServiceUsuario
    {
        public RespuestaLogin iniciarSesion(string username, string password)
        {
            RespuestaLogin repuestaPeticion = UsuarioDAO.verificarUsuario(username,
                                                                            password);
            return repuestaPeticion;
        }

        public Mensaje registrarUsuario(string nombre, string apellidos, string username, string password)
        {
            Usuario usuarioRegistro = new Usuario();
            usuarioRegistro.Nombre = nombre;
            usuarioRegistro.Apellidos = apellidos;
            usuarioRegistro.Username = username;
            usuarioRegistro.Password = password;
            Mensaje respuestaRegistro = UsuarioDAO.insertarUsuario(usuarioRegistro);
            return respuestaRegistro;
        }

        public Mensaje eliminarUsuario(int idUsuario)
        {
            Mensaje respuestaEliminacion = UsuarioDAO.eliminarUsuario(idUsuario);
            return respuestaEliminacion;
        }

        public Mensaje editarUsuario(int idUsuario, string nombre, string apellidos, string password)
        {
            Mensaje respestaEdicion = UsuarioDAO.editarUsuario(idUsuario, nombre, apellidos, password);
            return respestaEdicion;
        }

        public List<Usuario> obtenerUsuarios()
        {
            List<Usuario> usuarios = UsuarioDAO.obtenerUsuarios();
            return usuarios;
        }
    }
}
