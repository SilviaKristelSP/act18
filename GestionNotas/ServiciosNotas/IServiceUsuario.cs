﻿using ServiciosNotas.Modelo.Poco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiciosNotas
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IServiceUsuario" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServiceUsuario
    {
        [OperationContract]
        RespuestaLogin iniciarSesion(string username, string password);

        [OperationContract]
        Mensaje registrarUsuario(string nombre, string apellidos, string username, string password);

        [OperationContract]
        Mensaje eliminarUsuario(int idUsuario);
        
        [OperationContract]
        Mensaje editarUsuario(int idUsuario, string nombre, string apellidos, string password);

        [OperationContract]
        List<Usuario> obtenerUsuarios();


    }
}
