﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosNotas.Modelo.Poco
{
    public class RespuestaLogin
    {
        public Boolean UsuarioCorrecto { get; set; }
        public string Mensaje { get; set; }
        public Usuario InformacionUsuario { get; set; }
    }
}