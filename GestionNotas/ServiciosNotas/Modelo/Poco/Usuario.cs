﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosNotas.Modelo.Poco
{
    public class Usuario
    {
        public int IdUsuario { get; set; }

        public string Nombre { get; set; }

        public string Apellidos { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
        
    }
}