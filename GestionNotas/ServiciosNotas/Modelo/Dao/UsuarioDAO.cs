﻿using MySql.Data.MySqlClient;
using ServiciosNotas.Modelo.Poco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosNotas.Modelo.Dao
{
    public class UsuarioDAO
    {

        public static RespuestaLogin verificarUsuario(string username, 
                                                      string password)
        {
            RespuestaLogin respuesta = new RespuestaLogin();
            MySqlConnection conexionBD = ConnectionUtil.obtenerConexion();
            if (conexionBD != null)
            {
                try
                {
                    string consulta = string.Format("SELECT * FROM usuario " +
                                                "WHERE username = '{0}' AND " +
                                                "password = '{1}'", username, password);
                    MySqlCommand mySqlCommand = new MySqlCommand(consulta, conexionBD);
                    MySqlDataReader respuestaBD = mySqlCommand.ExecuteReader();
                    if (respuestaBD.Read())
                    {
                        respuesta.UsuarioCorrecto = true;
                        respuesta.Mensaje = "Usuario verificado correctamente";
                        Usuario usuario = new Usuario();
                        usuario.IdUsuario = ((respuestaBD.IsDBNull(0)) ? 0 : respuestaBD.GetInt32(0));
                        usuario.Nombre = ((respuestaBD.IsDBNull(1)) ? "" : respuestaBD.GetString(1));
                        usuario.Apellidos = (respuestaBD.IsDBNull(2) ? "" : respuestaBD.GetString(2));
                        usuario.Username = ((respuestaBD.IsDBNull(3)) ? "" : respuestaBD.GetString(3));
                        usuario.Password = ((respuestaBD.IsDBNull(4)) ? "" : respuestaBD.GetString(4));
                        respuesta.InformacionUsuario = usuario;
                    }
                    else
                    {
                        respuesta.UsuarioCorrecto = false;
                        respuesta.Mensaje = "Usuario y/o contraseña incorrectos...";
                    }
                }
                catch (Exception ex)
                {
                    respuesta.UsuarioCorrecto = false;
                    respuesta.Mensaje = ex.Message;
                }
            }
            else
            {
                respuesta.UsuarioCorrecto = false;
                respuesta.Mensaje = "Por el momento no hay servicio disponible...";
            }
            return respuesta;
        }

        public static Mensaje insertarUsuario(Usuario usuarioRegistro)
        {
            Mensaje mensaje = new Mensaje();
            MySqlConnection conexionBD = ConnectionUtil.obtenerConexion();
            if(conexionBD != null)
            {
                try
                {
                    string sentencia = "INSERT INTO usuario (nombre, apellidos, username, password) "+
                                       "VALUES(@nombre,@apellidos,@username,@password)";
                    MySqlCommand mySqlCommand = new MySqlCommand(sentencia, conexionBD);
                    mySqlCommand.Parameters.AddWithValue("@nombre", usuarioRegistro.Nombre);
                    mySqlCommand.Parameters.AddWithValue("@apellidos", usuarioRegistro.Apellidos);
                    mySqlCommand.Parameters.AddWithValue("@username", usuarioRegistro.Username);
                    mySqlCommand.Parameters.AddWithValue("@password", usuarioRegistro.Password);
                    mySqlCommand.Prepare();
                    int filasAfectadas = mySqlCommand.ExecuteNonQuery();
                    if(filasAfectadas > 0)
                    {
                        mensaje.Error = false;
                        mensaje.MensajeRespuesta = "Usuario registrado con éxito";
                    }
                    else
                    {
                        mensaje.Error = true;
                        mensaje.MensajeRespuesta = "Error al registrar el usuario";
                    }

                }catch (Exception ex)
                {
                    mensaje.Error = true;
                    mensaje.MensajeRespuesta = ex.Message;
                }
            }
            else
            {
                mensaje.Error = true;
                mensaje.MensajeRespuesta = "Por el momento no hay conexión con los servicios...";
            }
            return mensaje;
        }

        public static Mensaje eliminarUsuario(int idUsuario)
        {
            Mensaje mensaje = new Mensaje();
            MySqlConnection conexionBD = ConnectionUtil.obtenerConexion();
            if(conexionBD != null)
            {
                try
                {
                    string sentencia = "DELETE FROM usuario WHERE idUsuario = @idUsuario";
                    MySqlCommand mySqlCommand = new MySqlCommand(sentencia, conexionBD);
                    mySqlCommand.Parameters.AddWithValue("@idUsuario", idUsuario);
                    mySqlCommand.Prepare();
                    int filasAfectadas = mySqlCommand.ExecuteNonQuery();
                    if(filasAfectadas > 0)
                    {
                        mensaje.Error = false;
                        mensaje.MensajeRespuesta = "Usuario eliminado con éxito";
                    }
                    else
                    {
                        mensaje.Error = true;
                        mensaje.MensajeRespuesta = "Error al eiminar el usuario";
                    }
                }
                catch (Exception ex)
                {
                    mensaje.Error = true;
                    mensaje.MensajeRespuesta = ex.Message;
                }
            }
            else
            {
                mensaje.Error = true;
                mensaje.MensajeRespuesta = "Por el momento no hay conexión con los servicios...";
            }
            return mensaje;
        }

        public static Mensaje editarUsuario(int idUsuario, string nombre, string apellidos, string password)
        {
            Mensaje mensaje = new Mensaje();
            MySqlConnection conexionBD = ConnectionUtil.obtenerConexion();
            if(conexionBD != null)
            {
                try
                {
                    string sentencia = "UPDATE usuario SET nombre= @nombre,  apellidos = @apellidos, password = @password WHERE idUsuario = @idUsuario";
                    MySqlCommand mySqlCommand = new MySqlCommand(sentencia, conexionBD);
                    mySqlCommand.Parameters.AddWithValue("@nombre", nombre);
                    mySqlCommand.Parameters.AddWithValue("@apellidos", apellidos);
                    mySqlCommand.Parameters.AddWithValue("@password", password);
                    mySqlCommand.Parameters.AddWithValue("@idUsuario", idUsuario);
                    mySqlCommand.Prepare();
                    int filasAfectadas = mySqlCommand.ExecuteNonQuery();
                    if(filasAfectadas > 0)
                    {
                        mensaje.Error = false;
                        mensaje.MensajeRespuesta = "Usuario editado con éxito";
                    }
                    else
                    {
                        mensaje.Error = true;
                        mensaje.MensajeRespuesta = "Error al editar el usuario";
                    }
                }
                catch (Exception ex)
                {
                    mensaje.Error = true;
                    mensaje.MensajeRespuesta = ex.Message;
                }
            }
            else
            {
                mensaje.Error = true;
                mensaje.MensajeRespuesta = "Por el momento no hay conexión con los servicios...";
            }
            return mensaje;
        }

        public static List<Usuario> obtenerUsuarios()
        {
            List <Usuario> usuariosBD = new List<Usuario>();
            MySqlConnection conexionBD = ConnectionUtil.obtenerConexion();
            if(conexionBD != null)
            {
                try
                {
                    string consulta = "SELECT * FROM usuario";
                    MySqlCommand mySqlCommand = new MySqlCommand(consulta, conexionBD);
                    MySqlDataReader respuestaBD = mySqlCommand.ExecuteReader();                    
                    while (respuestaBD.Read())
                    {
                        Usuario usuario = new Usuario();
                        usuario.IdUsuario = ((respuestaBD.IsDBNull(0)) ? 0 : respuestaBD.GetInt32(0));
                        usuario.Nombre = ((respuestaBD.IsDBNull(1)) ? "" : respuestaBD.GetString(1));
                        usuario.Apellidos = (respuestaBD.IsDBNull(2) ? "" : respuestaBD.GetString(2));
                        usuario.Username = ((respuestaBD.IsDBNull(3)) ? "" : respuestaBD.GetString(3));
                        usuario.Password = ((respuestaBD.IsDBNull(4)) ? "" : respuestaBD.GetString(4));
                        usuariosBD.Add(usuario);
                    }

                } 
                catch (Exception ex)
                {
                    usuariosBD = null;
                }
            }
            else
            {
                usuariosBD = null;
            }
            return usuariosBD;
        }
    }

}