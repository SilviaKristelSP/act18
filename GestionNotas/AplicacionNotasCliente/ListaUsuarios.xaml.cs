﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ServiceReferenceUsuario;

namespace AplicacionNotasCliente
{
    /// <summary>
    /// Lógica de interacción para ListaUsuarios.xaml
    /// </summary>
    public partial class ListaUsuarios : Window
    {
        ServiceUsuarioClient servicioUsuario;
        Usuario[] usuarios;

        public ListaUsuarios()
        {
            InitializeComponent();
            servicioUsuario = new ServiceUsuarioClient();
            cargarUsuarios();
        }

        private async void cargarUsuarios()
        {
            if(servicioUsuario != null)
            {
                usuarios = await servicioUsuario.obtenerUsuariosAsync();
                if(usuarios != null)
                {
                    dgUsuarios.ItemsSource = usuarios;
                }
                else
                {
                    MessageBox.Show("No hay usuarios registrados", "Sin usuarios");
                }
            }
        }

        private void clicNuevoUsuario(object sender, RoutedEventArgs e)
        {
            FormularioUsuario formularioUsuario = new FormularioUsuario("registrar", null);
            formularioUsuario.Show();
            this.Close();
        }

        private void clicEditarUsuario(object sender, RoutedEventArgs e)
        {
            Usuario usuarioSeleccionado = (Usuario) dgUsuarios.SelectedItem;
            if(usuarioSeleccionado != null)
            {
                FormularioUsuario formularioUsuario = new FormularioUsuario("editar", usuarioSeleccionado);
                formularioUsuario.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Seleccione un usuario para realizar la edición", "Operación inválida");
            }
            
        }

        private async void clicEliminarUsuario(object sender, RoutedEventArgs e)
        {
            Usuario usuarioSeleccionado = (Usuario)dgUsuarios.SelectedItem;
            if (usuarioSeleccionado != null)
            {
                if(servicioUsuario != null)
                {
                    Mensaje respuesta = await servicioUsuario.eliminarUsuarioAsync(usuarioSeleccionado.IdUsuario);
                    if(respuesta.Error == false)
                    {
                        MessageBox.Show(respuesta.MensajeRespuesta, "Eliminación exitosa");
                        cargarUsuarios();
                    }
                    else
                    {
                        MessageBox.Show(respuesta.MensajeRespuesta, "Eliminación fallida");
                    }
                }
            }
            else
            {
                MessageBox.Show("Seleccione un usuario para realizar la eliminación", "Operación inválida");
            }
        }
    }
}
