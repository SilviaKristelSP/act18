﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ServiceReferenceUsuario;
using AplicacionNotasCliente.poco;

namespace AplicacionNotasCliente
{

    public partial class Login : Window
    {
        ServiceUsuarioClient servicioUsuario;
        RespuestaLogin respuestaLogin;

        public Login()
        {
            InitializeComponent();
            servicioUsuario = new ServiceUsuarioClient();
            respuestaLogin = new RespuestaLogin();
        }

        private async void clicIniciarSesion(object sender, RoutedEventArgs e)
        {
            if (pbContrasenia.Password != "" && tbNombreUsuario.Text != "")
            {  
                if(servicioUsuario != null)
                {
                    respuestaLogin = await servicioUsuario.iniciarSesionAsync(tbNombreUsuario.Text, pbContrasenia.Password);
                    if (respuestaLogin.UsuarioCorrecto == true)
                    {
                        MessageBox.Show(respuestaLogin.Mensaje, "Login exitoso");
                        ListaUsuarios listaUsuarios = new ListaUsuarios();
                        listaUsuarios.Show();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show(respuestaLogin.Mensaje, "Login fallido");
                    }
                }
            }
            else
            {
                MessageBox.Show("El formulario debe estar lleno para continuar", "Login fallido");
            }
        }
        
    }
}
