﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionNotasCliente.poco
{
    internal class MensajeModelo
    {
        public Boolean Error { get; set; }

        public String MensajeRespuesta { get; set; }
    }
}
