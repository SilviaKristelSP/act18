﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AplicacionNotasCliente.poco;

namespace AplicacionNotasCliente.poco
{
    internal class RespuestaLoginModelo
    {
        public Boolean UsuarioCorrecto { get; set; }
        public string Mensaje { get; set; }
        public UsuarioModelo InformacionUsuario { get; set; }
    }
}
