﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionNotasCliente.poco
{
    internal class UsuarioModelo
    {
        public int IdUsuario { get; set; }

        public string Nombre { get; set; }

        public string Apellidos { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
