﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ServiceReferenceUsuario;

namespace AplicacionNotasCliente
{
    /// <summary>
    /// Lógica de interacción para FormularioUsuario.xaml
    /// </summary>
    public partial class FormularioUsuario : Window
    {
        String operacionFormulario;
        ServiceUsuarioClient servicioUsuario;
        Usuario usuarioFormulario;

        public FormularioUsuario(String operacion, Usuario usuario)
        {
            InitializeComponent();

            servicioUsuario = new ServiceUsuarioClient();
            operacionFormulario = operacion;
            usuarioFormulario = usuario;

            if (operacion == "registrar")
            {
                lbTituloUsuario.Content = "Registrar Usuario";
            }
            else
            {
                lbTituloUsuario.Content = "Editar Usuario";
                tbUsername.Text = usuario.Username;
                tbUsername.IsReadOnly = true;                
                tbNombre.Text = usuario.Nombre;
                tbApellido.Text = usuario.Apellidos;
                tbPassword.Text = usuario.Password;
            }
        }

        private async void clicGuardar(object sender, RoutedEventArgs e)
        {
            if (verificarFormulario())
            {
                if (servicioUsuario != null)
                {
                    Mensaje respuesta = new Mensaje();
                    if (operacionFormulario == "registrar")
                    {
                        respuesta = await servicioUsuario.registrarUsuarioAsync(tbNombre.Text, tbApellido.Text, tbUsername.Text, tbPassword.Text);
                        if(respuesta.Error == false)
                        {
                            MessageBox.Show(respuesta.MensajeRespuesta, "Registro exitoso");                            
                        }
                        else
                        {
                            MessageBox.Show(respuesta.MensajeRespuesta, "Registro fallido");
                        }
                    }
                    else if (operacionFormulario == "editar")
                    {
                        usuarioFormulario.Nombre = tbNombre.Text;
                        usuarioFormulario.Apellidos = tbApellido.Text;
                        usuarioFormulario.Password = tbPassword.Text;
                        respuesta = await servicioUsuario.editarUsuarioAsync(usuarioFormulario.IdUsuario, usuarioFormulario.Nombre, 
                                                                            usuarioFormulario.Apellidos, usuarioFormulario.Password);
                        if (respuesta.Error == false)
                        {
                            MessageBox.Show(respuesta.MensajeRespuesta, "Edición exitosa");
                        }
                        else
                        {
                            MessageBox.Show(respuesta.MensajeRespuesta, "Edición fallida");
                        }
                    }
                }
                ListaUsuarios listaUsuarios = new ListaUsuarios();
                listaUsuarios.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("El formulario debe estar completo para continuar", "Error");
            }
        }

        private bool verificarFormulario()
        {
            bool valorVerificacion = true;
            if(tbUsername.Text == "")
            {
                valorVerificacion = false;
            }
            else if(tbNombre.Text == "")
            {
                valorVerificacion = false;
            }
            else if(tbApellido.Text == "")
            {
                valorVerificacion = false;
            }
            else if(tbPassword.Text == "")
            {
                valorVerificacion = false;
            }
            return valorVerificacion;
        }
    }
}
